import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="filedownloader",
    version="0.0.1",
    author="ApeWithCompiler",
    author_email="",
    description="A small class for file download",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ApeWithCompiler/py-filedownloader",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
       'requests'
    ],
)