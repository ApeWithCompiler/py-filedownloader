#!/usr/bin/python

import requests
import shutil


class Downloader(object):
    """
    Basic downloader implemantation
    """
    def __init__(self):
        super(Downloader, self).__init__()
        pass

    def download_file(self, url, path):
        response = requests.get(url, stream=True)
        if response.ok:
            with open(path, 'wb') as file:
                response.raw.decode_content = True
                shutil.copyfileobj(response.raw, file)


class SessionDownloader(object):
    """
    A downloader implementation wrapping around a session.
    This allows configuring cookies/headers/proxies as
    python requests offers.
    """
    def __init__(self, session):
        super(SessionDownloader, self).__init__()
        self.session = session

    def download_file(self, url, path):
        response = self.session.get(url, stream=True)
        if response.ok:
            with open(path, 'wb') as file:
                response.raw.decode_content = True
                shutil.copyfileobj(response.raw, file)