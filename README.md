# Py Filedownloader

A simple file download wrapper

## Installation

```
git clone https://gitlab.com/ApeWithCompiler/py-filedownloader
pip3 install -e py-filedownloader
```

## Usage

``` python
from filedownloader import Downloader, SessionDownloader

URL = "https://example.com/files/cutekitten.mp4"
FILE = "/tmp/cutekitten.mp4"

# Simple download
d = Downloader()
d.download_file(URL, FILE)


# Use a session
import requests

s = requests.session()
sd = SessionDownloader(s)
sd.download_file(URL, FILE)
```